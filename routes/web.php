<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('members.create');
})->middleware('auth');;

Route::resource('register','RegisterController')->middleware('auth');;

// Routes for the payments
Route::get('pay/transactions', 'PayController@create')->middleware('auth');;
Route::get('pay/invoices', 'PayController@invoices')->middleware('auth');;
Route::get('pay/make', 'PayController@make')->middleware('auth');;

// Route for member management
Route::resource('assign', 'AssignMembershipController')->middleware('auth');;

// Route for plans management
Route::resource('plans', 'PlansController')->middleware('auth');;

// Authentication routes
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// Send invoice to
Route::post('send-invoice', 'PayController@send');

Route::resource('page','PageController');
