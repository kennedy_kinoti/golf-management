@extends('layouts.index')

@section('content')
  <div class="container-fluid">
    <small>Assign Member a Plan</small><br/>
    <form method="post" action="{{url('assign')}}" enctype="multipart/form-data">
    {{ csrf_field() }}

      <div class="row">
        <div class="col-md-4"></div>
        <div class="form-group col-md-4">
          <label for="Name">Member Name:</label>
          <select class="form-control" name="user_id" id="member">
            <option selected disabled>Select a Member</option>
            @foreach($members as $member)
              <option value="{{$member['id']}}">{{$member['name']}}</option>
            @endforeach
          </select>
        </div>
      </div>

      <div class="row">
        <div class="col-md-4"></div>
        <div class="form-group col-md-4">
          <label for="Plan">Plan:</label>
          <select class="form-control" name="plan_id" id="plan">
            <option selected disabled>Select a Plan<option>
            @foreach($plans as $plan)
              <option value="{{$plan['id']}}">{{$plan['plan_name']}}</option>
            @endforeach
          </select>
        </div>
      </div>
        
      <div class="row">
        <div class="col-md-4"></div>
        <div class="form-group col-md-4">
          <button type="submit" class="btn btn-success">Submit</button>
        </div>
      </div>
    </form>
  </div>
  <script type="text/javascript">  
      $('#datepicker').datepicker({ 
          autoclose: true,   
          format: 'dd-mm-yyyy'  
        });  
  </script>
@endsection