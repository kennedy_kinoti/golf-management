@extends('layouts.index')

@section('content')

  <div class="container-fluid">
  <small>View Members in Plans</small><br/>
    @if (\Session::has('success'))
      <div class="alert alert-success">
        <p>{{ \Session::get('success') }}</p>
      </div><br />
    @endif
    <table class="table table-striped">
      <thead>
        <tr>
          <th>Member Name</th>
          <th>Plan</th>
          <th>Duration</th>
          <th colspan="2">Action</th>
        </tr>
      </thead>
      <tbody>
        
        @foreach($assigned_members as $member)
        
        <tr>
          <td>{{$member->name}}</td>
          <td>{{$member->plan_name}}</td>
          <td>{{ '1 Month' }}</td>
          <td><a href="{{action('PlansController@edit', $member->id)}}" class="btn btn-warning">Edit</a></td>
          <td>
            <form action="{{action('PlansController@destroy', $member->id)}}" method="post">
            {{ csrf_field() }}
              <input name="_method" type="hidden" value="DELETE">
              <button class="btn btn-danger" type="submit">Delete</button>
            </form>
          </td>
        
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
  <!-- Footer -->
  <div class="footer"><footer>Strathmore University</footer></div>
  <!-- End of footer -->
@endsection