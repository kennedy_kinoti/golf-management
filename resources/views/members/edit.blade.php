<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>SU Golf</title>
        <link rel="stylesheet" href="{{asset('css/app.css')}}">
    </head>

    <body>
  <!-- header -->
  @include('template.header')
  <!-- end of header -->

    <div class="container-fluid">
        <h2>Strathmore Golf Management</h2><small>Edit</small><br/>
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div><br />
        @endif
        <form method="post" action="{{action('RegisterController@update', $id)}}">
            {{csrf_field()}}
            <input name="_method" type="hidden" value="PATCH">

            <div class="row">
                <div class="col-md-4"></div>
                <div class="form-group col-md-4">
                    <label for="name">Name:</label>
                    <input type="text" class="form-control" name="name" value="{{$passport->name}}">
                </div>
            </div>

            <div class="row">
                <div class="col-md-4"></div>
                <div class="form-group col-md-4">
                    <label for="price">Date:</label>
                    <input type="text" class="form-control" name="date" value="{{$passport->date}}">
                </div>
            </div>

        <div class="row">
            <div class="col-md-4"></div>
            <div class="form-group col-md-4">
                <label for="price">Email:</label>
                <input type="text" class="form-control" name="email" value="{{$passport->email}}">
            </div>
        </div>

        <div class="row">
            <div class="col-md-4"></div>
            <div class="form-group col-md-4">
                <label for="price">Phone Number:</label>
                <input type="text" class="form-control" name="number" value="{{$passport->number}}">
            </div>
        </div>

        <div class="row">
            <div class="col-md-4"></div>
                <div class="form-group col-md-4">
                    <label for="level">Proficiency</label>
                    <select name="office" class="form-control" required>
                    <option value="{{$passport->office}}" disabled selected>{{$passport->office}}</option>
                    <option value="Beginner">Beginner</option>
                    <option value="Intermediate">Intermediate</option>
                    <option value="Semi-Pro">Semi-Pro</option>  
                    <option value="Legend">Ledgend</option>  
                    </select>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4"></div>
                <div class="form-group col-md-4">
                    <button type="submit" class="btn btn-success">Update Product</button>
            </div>
        </div>

      </form>
    </div>
  </body>
</html>
