@extends('layouts.index')

@section('content')
  <div class="container-fluid">
    <br />
    @if (\Session::has('success'))
      <div class="alert alert-success">
        <p>{{ \Session::get('success') }}</p>
      </div><br />
    @endif
    <table class="table table-striped">
      <thead>
        <tr>
          <th>ID</th>
          <th>Name</th>
          <th>Date</th>
          <th>Email</th>
          <th>Phone Number</th>
          <th>Proficiency</th>
          <th colspan="2">Action</th>
        </tr>
      </thead>
      <tbody>
        
        @foreach($passports as $passport)
        
        <tr>
          <td>{{$passport['id']}}</td>
          <td>{{$passport['name']}}</td>
          <td>{{$passport['date']}}</td>
          <td>{{$passport['email']}}</td>
          <td>{{$passport['number']}}</td>
          <td>{{$passport['office']}}</td>
          
          <td><a href="{{action('RegisterController@edit', $passport['id'])}}" class="btn btn-warning">Edit</a></td>
          <td>
            <form action="{{action('RegisterController@destroy', $passport['id'])}}" method="post">
            {{ csrf_field() }}
              <input name="_method" type="hidden" value="DELETE">
              <button class="btn btn-danger" type="submit">Delete</button>
            </form>
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
  <!-- Footer -->
  <div class="footer"><footer>Strathmore University</footer></div>
  <!-- End of footer -->
@endsection
