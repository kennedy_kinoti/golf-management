@extends('layouts.index')

@section('content')
<div class="container-fluid">
  <small>Member Registration</small><br/>
  <form method="post" action="{{url('register')}}" enctype="multipart/form-data">
  {{ csrf_field() }}
    <div class="row">
      <div class="col-md-4"></div>
      <div class="form-group col-md-4">
        <label for="Name">Name:</label>
        <input type="text" class="form-control" name="name" required>
      </div>
    </div>
    <div class="row">
      <div class="col-md-4"></div>
        <div class="form-group col-md-4">
          <label for="Email">Email:</label>
          <input type="text" class="form-control" name="email" required>
        </div>
      </div>
    <div class="row">
      <div class="col-md-4"></div>
        <div class="form-group col-md-4">
          <label for="Number">Phone Number:</label>
          <input type="text" class="form-control" name="number" required>
        </div>
      </div>
    <div class="row">
      <div class="col-md-4"></div>
      <div class="form-group col-md-4">
        <label for="passport">Passport Photo</label>
        <input type="file" class="form-control" name="filename">    
      </div>
    </div>
    <div class="row">
      <div class="col-md-4"></div>
      <div class="form-group col-md-4">
        <label for="date">Date : </label>  
        <input class="date form-control" value="<?php echo date('d-m-Y')?>" type="text" id="datepicker" name="date" required>   
      </div>
    </div>
      <div class="row">
      <div class="col-md-4"></div>
        <div class="form-group col-md-4">
            <label for="level">Proficiency</label>
            <select name="office" class="form-control" required>
              <option value="Beginner">Beginner</option>
              <option value="Intermediate">Intermediate</option>
              <option value="Semi-Pro">Semi-Pro</option>  
              <option value="Legend">Ledgend</option>  
            </select>
        </div>
    </div>
    <div class="row">
      <div class="col-md-4"></div>
      <div class="form-group col-md-4">
        <button type="submit" class="btn btn-success">Submit</button>
      </div>
    </div>
  </form>
</div>
<script type="text/javascript">  
    $('#datepicker').datepicker({ 
        autoclose: true,   
        format: 'dd-mm-yyyy'  
      });  
</script>
@endsection
