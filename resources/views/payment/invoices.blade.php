@extends('layouts.index')

@section('content')

  <div class="container-fluid">
    <small>View Invoices</small><br/>
    @if (\Session::has('success'))
      <div class="alert alert-success">
        <p>{{ \Session::get('success') }}</p>
      </div><br />
    @endif
    <table class="table table-hover">
      <thead>
        <tr>
          <th>ID</th>
          <th>Member</th>
          <th>Date</th>
          <th>Package</th>
          <th>Amount</th>
          <th>Due Date</th>
          <th>Proficiency</th>
          <th colspan="2">Action</th>
        </tr>
      </thead>
      <tbody>
        
        <tr>
          <td>1</td>
          <td>Kennedy</td>
          <td>31.4.2018</td>
          <td>Gold Intermediate</td>
          <td>3000</td>
          <td class="bg-danger">31 April</td>
          <td>Beginner</td>
          <td><a href="{{action('PayController@create')}}" class="btn btn-success">Pay Bill</a></td>
        </tr>
      </tbody>
    </table>
  </div>
  <!-- Footer -->
  <div class="footer"><footer>Strathmore University</footer></div>
  <!-- End of footer -->
@endsection