@extends('layouts.index')

@section('content')
  <div class="container-flud">
    <small>View Payments</small><br/>
    @if (\Session::has('success'))
      <div class="alert alert-success">
        <p>{{ \Session::get('success') }}</p>
      </div><br />
    @endif
    <table class="table table-striped">
      <thead>
        <tr>
          <th>ID</th>
          <th>Date</th>
          <th>Package</th>
          <th>Amount</th>
          <th>Due Date</th>
          <th>Date Paid</th>
          <th>Status</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>1</td>
          <td>31.4.2018</td>
          <td>Gold Intermediate</td>
          <td>3000</td>
          <td class="bg-success">31 April</td>
          <td class="bg-success">31 April</td>
          <td> Successful </td>
        </tr>
      </tbody>
    </table>
  </div>
  <!-- Footer -->
  <div class="footer"><footer>Strathmore University</footer></div>
  <!-- End of footer -->
@endsection