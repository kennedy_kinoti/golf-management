@extends('layouts.index')

@section('content')
    <div class="container-fluid">
        <small>Edit Plans</small><br/>
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div><br />
        @endif
        <form method="post" action="{{action('PlansController@update', $id)}}">
            {{csrf_field()}}
            <input name="_method" type="hidden" value="PATCH">

            <div class="row">
                <div class="col-md-4"></div>
                <div class="form-group col-md-4">
                    <label for="name">Plan Name:</label>
                    <input type="text" class="form-control" name="plan_name" required value="{{$plan->plan_name}}">
                </div>
            </div>

            <div class="row">
                <div class="col-md-4"></div>
                <div class="form-group col-md-4">
                    <label for="price">Amount:</label>
                    <input type="text" class="form-control" name="amount" required min="3000" value="{{$plan->amount}}">
                </div>
            </div>

            <div class="row">
                <div class="col-md-4"></div>
                    <div class="form-group col-md-4">
                        <label for="level">Proficiency</label>
                        <select name="office" class="form-control" required>
                        <option value="{{$plan->level}}" disabled selected>{{$plan->level}}</option>
                        <option value="Beginner">Beginner</option>
                        <option value="Intermediate">Intermediate</option>
                        <option value="Semi-Pro">Semi-Pro</option>  
                        <option value="Legend">Ledgend</option>  
                        </select>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4"></div>
                    <div class="form-group col-md-4">
                        <button type="submit" class="btn btn-success">Update Product</button>
                </div>
            </div>
        </form>
    </div>
@endsection
