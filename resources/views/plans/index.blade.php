@extends('layouts.index')

@section('content')

  <div class="container-fluid">
  <small>View Plans</small><br/>
    @if (\Session::has('success'))
      <div class="alert alert-success">
        <p>{{ \Session::get('success') }}</p>
      </div><br />
    @endif
    <table class="table table-striped">
      <thead>
        <tr>
          <th>Plan Name</th>
          <th>Amount</th>
          <th>Proficiency</th>
          <th colspan="2">Action</th>
        </tr>
      </thead>
      <tbody>
        @foreach($plans as $plan)
        <tr>
          <td>{{$plan['plan_name']}}</td>
          <td>{{$plan['amount']}}</td>
          <td>{{$plan['level']}}</td>
          
          <td><a href="{{action('PlansController@edit', $plan['id'])}}" class="btn btn-warning">Edit</a></td>
          <td>
            <form action="{{action('PlansController@destroy', $plan['id'])}}" method="post">
            {{ csrf_field() }}
              <input name="_method" type="hidden" value="DELETE">
              <button class="btn btn-danger" type="submit">Delete</button>
            </form>
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
  <!-- Footer -->
  <div class="footer"><footer>Strathmore University</footer></div>
  <!-- End of footer -->
@endsection