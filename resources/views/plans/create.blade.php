@extends('layouts.index')

@section('content')
<div class="container-fluid">
  <small>Create Plans</small><br/>
  <form method="post" action="{{url('plans')}}" enctype="multipart/form-data">
  {{ csrf_field() }}
    <div class="row">
      <div class="col-md-4"></div>
      <div class="form-group col-md-4">
        <label for="Plan Name">Plan Name:</label>
        <input type="text" class="form-control" name="plan_name" required>
      </div>
    </div>
    <div class="row">
      <div class="col-md-4"></div>
      <div class="form-group col-md-4">
        <label for="Plan Cost">Plan Cost:</label>
        <input type="number" class="form-control" name="amount" min="1000" required>
      </div>
    </div>
    <div class="row">
      <div class="col-md-4"></div>
        <div class="form-group col-md-4">
            <label for="level">Duration</label>
            <select name="level" class="form-control" required>
              <option value="1">1 Month</option>
              <option value="3">3 Months</option>
              <option value="12">Yearly</option>  
            </select>
        </div>
    </div>
    <div class="row">
      <div class="col-md-4"></div>
        <div class="form-group col-md-4">
            <label for="level">Proficiency</label>
            <select name="level" class="form-control" required>
              <option value="Beginner">Beginner</option>
              <option value="Intermediate">Intermediate</option>
              <option value="Semi-Pro">Semi-Pro</option>  
              <option value="Legend">Ledgend</option>  
            </select>
        </div>
    </div>
    <div class="row">
      <div class="col-md-4"></div>
      <div class="form-group col-md-4">
        <button type="submit" class="btn btn-success">Submit</button>
      </div>
    </div>
  </form>
</div>
<script type="text/javascript">  
    $('#datepicker').datepicker({ 
        autoclose: true,   
        format: 'dd-mm-yyyy'  
      });  
</script>
@endsection
<!-- Footer -->
<div class="footer"><footer>Strathmore University</footer></div>
<!-- End of footer -->