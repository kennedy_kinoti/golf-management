<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ 'Strathmore University Golf' }}</title>

        <!-- Bootstrap CSS CDN -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <!-- Our Custom CSS -->
        <link href="{{ asset('css/style.css') }}" rel="stylesheet">

        <!-- Scrollbar Custom CSS -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">

    </head>
    <body>



        <div class="wrapper">
            <!-- Sidebar Holder -->
            <nav id="sidebar">
                <div class="sidebar-header">
                    
                    <a class="navbar-brand" href="">
                        <img src="{{URL::asset('images/strathmore.png')}}" height="80" width="180"alt="Strathmore Golf">
                    </a>
                    
                </div>

                <ul class="list-unstyled components">
                    <p>-</p>

                    <li>
                        <a href="#plans" data-toggle="collapse" aria-expanded="false">Plans</a>
                        <ul class="collapse list-unstyled" id="plans">
                            <li><a href="{{url('/plans/create')}}">Create Plan</a></li>
                            <li><a href="{{url('/plans')}}">Manage Plans</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="#users" data-toggle="collapse" aria-expanded="false">Users</a>
                        <ul class="collapse list-unstyled" id="users">
                            <li><a href="{{url('/register/create')}}">Add a member</a></li>
                            <li><a href="{{url('/assign/create')}}">Assign Membership</a></li>
                            <li><a href="{{url('/assign')}}">Manage Memebers</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="#payment" data-toggle="collapse" aria-expanded="false">Payment</a>
                        <ul class="collapse list-unstyled" id="payment">
                            <li><a href="{{url('pay/make')}}">Make a Payment</a></li>
                            <li><a href="{{url('pay/invoices')}}">View Invoices</a></li>
                            <li><a href="{{url('pay/transactions')}}">View Payments</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="#">Settings</a>
                    </li>
                </ul>

                <ul class="list-unstyled CTAs">
                    <li><a href="http://www.strathmore.edu/" class="download">Book Lessons</a></li>
                    <li><a href="http://www.strathmore.edu/" class="article">Back to Strathmore Site</a></li>
                </ul>
            </nav>

            <!-- Page Content Holder -->
            <div id="content">

                <nav class="navbar navbar-default">
                    <div class="container-fluid">

                        <div class="navbar-header">
                            <button type="button" id="sidebarCollapse" class="btn btn-info navbar-btn">
                                <i class="glyphicon glyphicon-align-left"></i>
                                <span>Toggle Sidebar</span>
                            </button>
                        </div>

                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav navbar-right">
                                <!-- <li><a href="#">Page</a></li>
                                <li><a href="#">Page</a></li>
                                <li><a href="#">Page</a></li>
                                <li><a href="#">Page</a></li> -->
                                
                                <!-- Authentication Links -->
                                @guest
                                    <li><a href="{{ route('login') }}">Login</a></li>
                                    <li><a href="{{ route('register') }}">Register</a></li>
                                @else
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true" v-pre>
                                            {{ Auth::user()->name }} <span class="caret"></span>
                                        </a>

                                        <ul class="dropdown-menu">
                                            <li>
                                                <a href="{{ route('logout') }}"
                                                    onclick="event.preventDefault();
                                                            document.getElementById('logout-form').submit();">
                                                    Logout
                                                </a>

                                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                    {{ csrf_field() }}
                                                </form>
                                            </li>
                                        </ul>
                                    </li>
                                @endguest
                            </ul>
                        </div>
                    </div>
                </nav>

                <!-- Append content -->
                @yield('content')
                <!-- End of append content -->

            </div>
        </div>





        <!-- jQuery CDN -->
        <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
        <!-- Bootstrap Js CDN -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <!-- jQuery Custom Scroller CDN -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>

        <script type="text/javascript">
            $(document).ready(function () {
                $("#sidebar").mCustomScrollbar({
                    theme: "minimal"
                });

                $('#sidebarCollapse').on('click', function () {
                    $('#sidebar, #content').toggleClass('active');
                    $('.collapse.in').toggleClass('in');
                    $('a[aria-expanded=true]').attr('aria-expanded', 'false');
                });
            });
        </script>
    </body>
</html>
