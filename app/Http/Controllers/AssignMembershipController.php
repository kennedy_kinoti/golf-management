<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\AssignMembership;
use App\Plan;

use Illuminate\Support\Facades\DB;


class AssignMembershipController extends Controller
{

    // Form to assign members
    public function index()
    {   
        // $assigned_members = AssignMembership::all();

        $assigned_members = DB::table('assign_memberships')
            ->join('users', 'assign_memberships.user_id', '=', 'users.id')
            ->join('plans', 'assign_memberships.plan_id', '=', 'plans.id')
            ->get();

        return view('assign.index',compact('assigned_members'));
    }

    /**
     * Store a new resource in storage
     */
    public function store(Request $request)
    {
        $membership = new AssignMembership();
        $membership->user_id=$request->get('user_id');
        $membership->plan_id=$request->get('plan_id');
        $membership->save();
        
        return redirect('plans')->with('success', 'Member has been assigned a Plan');
    }

    public function create()
    {
        $members = User::all();
        $plans = Plan::all();
        return view('assign.create', compact('members','plans'));
    }
}
