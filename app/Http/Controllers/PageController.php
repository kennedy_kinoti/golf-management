<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{
    /**
     * Display all avalilable resources
     */

    public function index()
    {
        return view('page.index');
    }
}