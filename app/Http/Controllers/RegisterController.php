<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Passport;

class RegisterController extends Controller
{
    /**
     * Display all avalilable resources
     */

    public function index()
    {
        $passports=Passport::all();
        return view('members.index',compact('passports'));
    }
    /**
     * Create passport
     */
    public function create()
    {
        return view('members.create');
    }

    /**
     * Store a new resource in storage
     */
    public function store(Request $request)
    {
        if($request->hasfile('filename'))
        {
        $file = $request->file('filename');
        $name=time().$file->getClientOriginalName();
        $file->move(public_path().'/images/', $name);
        }

        $passport= new Passport();
        $passport->name=$request->get('name');
        $passport->email=$request->get('email');
        $passport->number=$request->get('number');
        $date=date_create($request->get('date'));
        $format = date_format($date,"Y-m-d");
        $passport->date = strtotime($format);
        $passport->office=$request->get('office');
        $passport->filename=$name;
        $passport->save();
        
        return redirect('register')->with('success', 'Information has been added');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $passport = Passport::find($id);
        return view('members.edit',compact('passport','id'));
    }

    //PassportCpntroller.php

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $passport= Passport::find($id);
        $passport->name=$request->get('name');
        $passport->email=$request->get('email');
        $passport->number=$request->get('number');
        $passport->office=$request->get('office');
        $passport->save();
        
        return redirect('register')->with('success', 'Information has been updated');
    }
    //PassportController.php
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $passport = passport::find($id);
        $passport->delete();
        return redirect('register')->with('success','Information has been  deleted');
    }
}
