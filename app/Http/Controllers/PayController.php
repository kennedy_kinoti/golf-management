<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Payment;

// URL::setRootControllerNamespace('App\Http\Contollers');

class PayController extends Controller
{
    /**
     * View all the transactions
     */
    public function index()
    {
        $member_package=Payment::all();
        return view('payment.index',compact('member_package'));
    }

    /**
     * Send an email with bill to be paid
     */

    public function create()
    {
        // return redirect('pay')->with('success', 'Email has been sent to ');
        return view('payment.transactions');
    }

    /**
     * Page to show unpaid invoices
     */
    public function invoices()
    {
        return view('payment.invoices');
    }

    /**
     * Page to display paid invoices
     */
    public function transactions()
    {
        return view('payment.transactions');
    }

    /**
     * Initiate payment processing
     * Send the invoice via email with action
     */
    public function make(){

        $live       = "0";

        $mer 		= "Twatrust"; 
        $oid 		= "invoice01"; 
        $inv        = "112020102292999";
        $ttl 		= "3000";
        $tel 		= "0715916968"; 
        $eml 		= "kinotikennedy@gmail.com"; 
        $vid		= "demo"; 
        $cur 		= "KES"; 
        $p1 		= "12";
        $p1 		= "3000";
        $p2 		= "0715916968";
        $p3 		= "invoice";
        $p4 		= "900";
        $cbk 		= $_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"];
        $cst 		= "1";
        $crl 		= "2"; 

        $datastring = $live.$oid.$inv.$ttl.$tel.$eml.$vid.$cur.$p1.$p2.$p3.$p4.$cbk.$cst.$crl;

        $cbk = urlencode($cbk);
        
        $hashkey ="demo";//use "demo" for testing where vid also is set to "demo"
        
        $generated_hash = hash_hmac('sha1',$datastring , $hashkey);

        $theUrl = "https://payments.ipayafrica.com/v3/ke?live=$live&oid=$oid&inv=$inv&ttl=$ttl&tel=$tel&eml=$eml&vid=$vid&p1=$p1&p2=$p2&p3=$p3&p4=$p4&crl=$crl&cbk=$cbk&cst=$cst&curr=$cur&hsh=$generated_hash";
        
        // header("location: $theUrl");
        
        return redirect($theUrl);
    }

    /**
     * Store the transaction in the database
     */
    public function store(Request $request){
        if($request->hasfile('filename'))
        {
        $file = $request->file('filename');
        $name=time().$file->getClientOriginalName();
        $file->move(public_path().'/images/', $name);
        }

        $payment= new payment();
        $payment->name=$request->get('name');
        $payment->email=$request->get('email');
        $payment->number=$request->get('number');
        $date=date_create($request->get('date'));
        $format = date_format($date,"Y-m-d");
        $payment->date = strtotime($format);
        $payment->office=$request->get('office');
        $payment->filename=$name;
        $payment->save();
        
        return redirect('register')->with('success', 'Information has been added');
    }

    public function process_payment(){
        //txncd refers to the
        //transaction code that the user entered (in the case of mobile money), 
        //or that was system generated (in the case of Kenswitch and VISA/Mastercard transactions).
        if(isset($_GET["txncd"])){
            $val           = "demo"; // the vendor ID
            //$last_id       = $_GET['p1'];
            
            //if telephone is different from the user telephone
            //use the number as registered by the mobile money / banking system
            $telephone = $_GET['p2'];
            $telephone = ($telephone == $_GET['msisdn_idnum']) ? $telephone : $_GET['msisdn_idnum'];

            //this is the amount of money that was sent via the mobile money transfer by the user. 
            //This comes as an integer and without the thousands (,) separator. 
            //You can use to authenticate against the amount that the user has checked out.
            //use ipay returned amount if different from amount posted
            $amount_posted = $_GET['p1'];
            $top_up_amount = $_GET['mc'];
            $top_up_amount = ($amount_posted == $top_up_amount) ? $amount_posted : $top_up_amount;
            $p4 = $_GET['p4'];


            //to authenticate order id again
            //map it to transaction order id
            $val1 = $_GET['id'];

            //invoice number returned as md5hash
            $val2 = $_GET['ivm'];
            $invoice_num = $_GET['p2'];
            $order_id = ($val2 == md5($invoice_num)) ? $invoice_num : $val2;

            /*
            * the following 6 values
            * are special unique browser specific identifier variables returned from the ipay system
            */
            $val3 = $_GET['qwh'];
            $val4 = $_GET['afd'];
            $val5 = $_GET['poi'];
            $val6 = $_GET['uyt'];
            $val7 = $_GET['ifd'];
            $val8 = $_GET['agt'];


            $ipnurl        = "https://www.ipayafrica.com/ipn/?vendor=".$val."&id=".$val1."&ivm=".$val2."&qwh=".$val3."&afd=".$val4."&poi=".$val5."&uyt=".$val6."&ifd=".$val7;

            if($this->config->item('vid') === "demo"){
                $status = "aei7p7yrx4ae34";
            } else {
                $fp            = fopen($ipnurl, "rb");
                $status        = stream_get_contents($fp, -1, -1);//$_GET['status'];
                fclose($fp);
            }

            //var_dump($val1);

            $payment_date = new DateTime();
            $payment_date = $payment_date->format('Y-m-d H:i:s');

            /*
            * Array of details to store to db
            *  order_id
            *  amount
            *  payment_status
            *  payment_date
            *  payment_status_message - added in switch block
            */
            $insert_arr = array(
                'order_id' => $val1,
                'user_id' => $_SESSION['id'],
                'amount' => $top_up_amount,
                'payment_status' => $status,
                'payment_date' => $payment_date
            );
            $message = '';
            switch($status){
                case 'fe2707etr5s4wq':
                    $message = 'Failed transaction. Not all parameters fulfilled. ';
                    $message .= 'A notification of this transaction sent to the merchant.';
                    break;
                case 'aei7p7yrx4ae34':
                    $message = 'Success: The transaction is valid. Therefore you can update this transaction.';
                    $message .= 'A notification of this transaction sent to the merchant.';
                    break;
                case 'bdi6p2yy76etrs':
                    $message = 'Pending: Incoming Mobile Money Transaction Not found. ';
                    $message .= 'Please try again in 5 minutes.';
                    break;
                case 'cr5i3pgy9867e1':
                    $message = 'Used: This code has been used already. ';
                    $message .= 'A notification of this transaction sent to the merchant.';
                    break;
                case 'dtfi4p7yty45wq':
                    $message = 'Less: The amount that you have sent via mobile money ';
                    $message .= 'is LESS than what was required to validate this transaction.';
                    break;
                case 'eq3i7p5yt7645e':
                    $message = 'More: The amount that you have sent via mobile money is ';
                    $message .= 'MORE than what was required to validate this transaction. ';
                    break;
                default:
                    $message = 'More: The amount that you have sent via mobile money is ';
                    $message .= 'MORE than what was required to validate this transaction. ';
            }
            $insert_arr['payment_status_message'] = $message;
            $id = $this->Payment_model->insertTransaction($insert_arr);
            $this->session->set_flashdata('flash_message', $message);
            if($id)
                redirect(site_url());
            else
                redirect('payment/payment');
        }
    }

}
