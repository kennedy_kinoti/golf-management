<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Plan;

class PlansController extends Controller
{
    /**
     * Display all avalilable resources
     */

    public function index()
    {
        $plans=Plan::all();
        return view('plans.index',compact('plans'));
    }
    /**
     * Create passport
     */
    public function create()
    {
        return view('plans.create');
    }

    /**
     * Store a new resource in storage
     */
    public function store(Request $request)
    {
        $plan= new Plan();
        $plan->plan_name=$request->get('plan_name');
        $plan->amount=$request->get('amount');
        $plan->level=$request->get('level');
        $plan->save();
        
        return redirect('plans')->with('success', 'Plan has been added');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $plan = Plan::find($id);
        return view('plans.edit',compact('plan','id'));
    }

    //PassportCpntroller.php

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   
        $plan= Plan::find($id);
        $plan->plan_name=$request->get('plan_name');
        $plan->amount=$request->get('amount');
        $plan->level=$request->get('level');
        $plan->save();
        
        return redirect('plans')->with('success', 'Information has been updated');
    }
    //PassportController.php
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $plan = Plan::find($id);
        $plan->delete();
        return redirect('plans')->with('success','Information has been  deleted');
    }
}
